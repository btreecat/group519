Student Information
-------------------
Stephen Tanner <stanner@vt.edu>
Partial help from Youssef Ben Jemia

How to execute the shell
------------------------

after building the shell with make esh, you can execute the
shell from the same dir with ./esh

Important Notes
---------------

I changed the compile options as stated on piazza to work with archlinux using
the -lfl flag instead of -ll

Additionally the project is hosted on bitbucket. I can give access to the TA's
after and account is setup for them to access it. 

The VCS I used was mercurial. 

The main issues I ran into besides not getting the project 100% working was lack
of resources (mainly time) due to also working a full time job. This caused me
to really be behind other groups as both my original and backup group mates
dropped/are dropping the class. 

The second partner Youssef stopped contributing on Sunday 9/21 and told me on
Tuesday 9/23 that he would no longer be working on the project with me due to
dropping the class in the near future.

Not being afforded that option I finally got over my fear of being able to turn
in something somewhat working and pressed on even if it mean using all of my
late days which I now have. 

So please have mercy on my busted code. 

Description of Base Functionality
---------------------------------

I just tried to break the command parts down into is basic elements. Then
classify the command. If I could classify the command I could figure out the
right execution path. 

This however was a lot more complex and involved than I anticipated. 

I was able to get the base functionality mostly working with some odd side
effects still not debugged. This is my fault due to mismanagement of time and
not taking time off work to see the TA's during their office hours. 

Honestly its kind of a haze at this point. I see the flow in my head and can
draw it in pictures but the code is all just blurring together at this point. 

Description of Extended Functionality
-------------------------------------
No time, none done =(

If I actually had a partner for this project and didn't work a full time job I
might have had time had I started earlier. But prior wedding engagement put the
kibash on that. 

List of Plugins Implemented
---------------------------

None. I would have loved to if I could have but I couldn't have so I didn't. My
bad. 
