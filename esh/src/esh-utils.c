/*
 * esh-utils.c
 * A set of utility routines to manage esh objects.
 *
 * Developed by Godmar Back for CS 3214 Fall 2009
 * Virginia Tech.
 */
#include <stdio.h>
#include <sys/types.h>
#include <dirent.h>
#include <dlfcn.h>
#include <limits.h>
#include "esh-sys-utils.h"
#include "esh.h"

//added
#include <unistd.h>
#include <sys/wait.h>


static const char rcsid [] = "$Id: esh-utils.c,v 1.5 2011/03/29 15:46:28 cs3214 Exp $";

const char const * INHOUSE[]={"stop", "kill", "bg", "fg", "jobs"};
const char *STATES[]={"Foreground","Running","Stopped", "Needs Terminal"};

/* List of loaded plugins */
struct list esh_plugin_list;

/* Create new command structure and initialize first command word,
 * and/or input or output redirect file. */
struct esh_command *
esh_command_create(char ** argv,
                   char *iored_input,
                   char *iored_output,
                   bool append_to_output)
{
    struct esh_command *cmd = malloc(sizeof *cmd);

    cmd->iored_input = iored_input;
    cmd->iored_output = iored_output;
    cmd->argv = argv;
    cmd->append_to_output = append_to_output;

    return cmd;
}

/* Create a new pipeline containing only one command */
struct esh_pipeline *
esh_pipeline_create(struct esh_command *cmd)
{
    struct esh_pipeline *pipe = malloc(sizeof *pipe);

    pipe->bg_job = false;
    cmd->pipeline = pipe;
    list_init(&pipe->commands);
    list_push_back(&pipe->commands, &cmd->elem);
    return pipe;
}

/* Complete a pipe's setup by copying I/O redirection information */
void
esh_pipeline_finish(struct esh_pipeline *pipe)
{
    if (list_size(&pipe->commands) == 0)
        return;

    struct esh_command *first;
    first = list_entry(list_front(&pipe->commands), struct esh_command, elem);
    pipe->iored_input = first->iored_input;

    struct esh_command *last;
    last = list_entry(list_back(&pipe->commands), struct esh_command, elem);
    pipe->iored_output = last->iored_output;
    pipe->append_to_output = last->append_to_output;
}

/* Create an empty command line */
struct esh_command_line *
esh_command_line_create_empty(void)
{
    struct esh_command_line *cmdline = malloc(sizeof *cmdline);

    list_init(&cmdline->pipes);
    return cmdline;
}

/* Create a command line with a single pipeline */
struct esh_command_line *
esh_command_line_create(struct esh_pipeline *pipe)
{
    struct esh_command_line *cmdline = esh_command_line_create_empty();

    list_push_back(&cmdline->pipes, &pipe->elem);
    return cmdline;
}

void simple_print(struct esh_command_line *cline) {
    struct list_elem * e = list_begin (&cline->pipes);
    struct esh_pipeline *pipe = list_entry(e, struct esh_pipeline, elem);
    struct list_elem * c = list_begin (&pipe->commands);
    struct esh_command *cmd = list_entry(c, struct esh_command, elem);
    char **p = cmd->argv;
    printf("COMMAND: %s \n", p[0]);
}


void simple_exec(struct esh_command_line *cline) {
    struct list_elem * e = list_begin (&cline->pipes);
    struct esh_pipeline *pipe = list_entry(e, struct esh_pipeline, elem);
    struct list_elem * c = list_begin (&pipe->commands);
    struct esh_command *cmd = list_entry(c, struct esh_command, elem);
    char **p = cmd->argv;
    //printf("COMMAND: %s", p[0]);
    pid_t child_pid = fork();
    if(child_pid == 0) {
        //do child stuff
        execvp(p[0], p);


    } else {
        //do parent stuff
        pid_t wait_child = wait(NULL);
        if (wait_child) {
            //do child stuff
        }

    }

}

//from Dr. B example code
void give_terminal_to(pid_t pgrp, struct termios *pg_tty_state) {
    esh_signal_block(SIGTTOU);
    int rc = tcsetpgrp(esh_sys_tty_getfd(), pgrp);
    if (rc == -1) {
        esh_sys_fatal_error("tcsetpgrp: ");
    }

    if (pg_tty_state) {
        esh_sys_tty_restore(pg_tty_state);
    }
    esh_signal_unblock(SIGTTOU);
}


/* Print esh_command structure to stdout */
void
esh_command_print(struct esh_command *cmd)
{
    char **p = cmd->argv;

    printf("  Command:");
    while (*p)
        printf(" %s", *p++);

    printf("\n");

    if (cmd->iored_output)
        printf("  stdout %ss to %s\n",
                cmd->append_to_output ? "append" : "write",
                cmd->iored_output);

    if (cmd->iored_input)
        printf("  stdin reads from %s\n", cmd->iored_input);
}

/* Print esh_pipeline structure to stdout */
void
esh_pipeline_print(struct esh_pipeline *pipe)
{
    int i = 1;
    struct list_elem * e = list_begin (&pipe->commands);

    printf(" Pipeline\n");
    for (; e != list_end (&pipe->commands); e = list_next (e)) {
        struct esh_command *cmd = list_entry(e, struct esh_command, elem);

        printf(" %d. ", i++);
        esh_command_print(cmd);
    }

    if (pipe->bg_job)
        printf("  - is a background job\n");
}

/* exec esh_command structure to stdout */
void
cmd_exec(struct esh_command *cmd)
{
    char **p = cmd->argv;

    //printf("  Command:");
    //while (*p)
    //    printf(" %s", *p++);

    pid_t child_pid = fork();
    if(child_pid == 0) {
        //do child stuff
        execvp(p[0], p);


    } else {
        //do parent stuff
        pid_t wait_cild = wait(NULL);
        if (wait_cild) {
        }
    }


    //printf("\n");

//    if (cmd->iored_output)
//        printf("  stdout %ss to %s\n",
//                cmd->append_to_output ? "append" : "write",
//                cmd->iored_output);
//
//    if (cmd->iored_input)
//        printf("  stdin reads from %s\n", cmd->iored_input);
}


/* exec esh_pipeline structure to stdout */
void
pipeline_exec(struct esh_pipeline *pipe)
{
    int i = 1;
    struct list_elem * e = list_begin (&pipe->commands);

    //printf(" Pipeline\n");
    for (; e != list_end (&pipe->commands); e = list_next (e)) {
        struct esh_command *cmd = list_entry(e, struct esh_command, elem);

        i++;

        //printf(" %d. ", i++);
        cmd_exec(cmd);
    }

    if (pipe->bg_job) {
        printf("  - is a background job\n");

    }
}



/*Add job to jobs list*/
void add_job(struct esh_pipeline* pipe)
{

    struct esh_job* j = (struct esh_job *) malloc (sizeof(struct esh_job));
    j->jobID = ++currJobID;
    j->pgrpID = pipe->pgrp;
    j->status = pipe->status;
    j->bg = pipe->bg_job;
    j->jobString = (pipe->status == STOPPED)? "Stopped"
                    :(pipe->status == NEEDSTERMINAL) ? "Waiting": "Running";
    list_push_back(&jobList,&(j->elem));


}

void esh_job_print()
{
    struct list_elem * e = list_begin (&jobList);

    printf("Jobs\n");
    for (; e != list_end (&jobList); e = list_next (e))
    {
        struct esh_job *job = list_entry(e, struct esh_job, elem);

        printf(" ------------- \n");

        printf("[%d]      %s",job->jobID, job->jobString);

    }
    printf("==========================================\n");
}

void jobs_print(struct list jobs) {

    struct list_elem *j = list_begin(&jobs);
    struct esh_pipeline *pipeline = list_entry(j, struct esh_pipeline, elem);

    printf("(");

    struct list_elem *e = list_begin(&pipeline->commands);
    for (; e != list_end(&pipeline->commands); e = list_next(e)) {

        struct esh_command *command = list_entry(e, struct esh_command, elem);

        char **params = command->argv;
        while (*params) {
            printf("%s ", *params);
            params++;
        }
        int cmds = list_size(&pipeline->commands);
        if ( cmds > 1) {
            printf("| ");
        }
    }

    printf(")\n");
}



void pipeline_print(struct esh_pipeline *pipeline) {
    printf("(");

    struct list_elem *e = list_begin(&pipeline->commands);
    for (; e != list_end(&pipeline->commands); e = list_next(e)) {

        struct esh_command *cmd = list_entry(e, struct esh_command, elem);

        char **argv = cmd->argv;
        while (*argv) {
            printf("%s ", *argv);
            fflush(stdout);
            argv++;
        }

        if (list_size(&pipeline->commands) > 1) {
            printf("| ");
        }
    }

    printf(")\n");
}


/* Exec esh_command_line structure to stdout */

void cmdline_exec(struct esh_command_line *cmdline)
{
    struct list_elem * e = list_begin (&cmdline->pipes);

    //printf("Command line\n");
    for (; e != list_end (&cmdline->pipes); e = list_next (e)) {
        struct esh_pipeline *pipe = list_entry(e, struct esh_pipeline, elem);
        //printf(" --------- ---- \n");
        pipeline_exec(pipe);
    }
    //printf("==========================================\n");
}


/* Print esh_command_line structure to stdout */
void
esh_command_line_print(struct esh_command_line *cmdline)
{
    struct list_elem * e = list_begin (&cmdline->pipes);

    printf("Command line\n");
    for (; e != list_end (&cmdline->pipes); e = list_next (e)) {
        struct esh_pipeline *pipe = list_entry(e, struct esh_pipeline, elem);

        printf(" ------------- \n");
        esh_pipeline_print(pipe);
    }
    printf("==========================================\n");
}


/* Deallocation functions. */
void
esh_command_line_free(struct esh_command_line *cmdline)
{
    struct list_elem * e = list_begin (&cmdline->pipes);

    for (; e != list_end (&cmdline->pipes); ) {
        struct esh_pipeline *pipe = list_entry(e, struct esh_pipeline, elem);
        e = list_remove(e);
        esh_pipeline_free(pipe);
    }
    free(cmdline);
}

void
esh_pipeline_free(struct esh_pipeline *pipe)
{
    struct list_elem * e = list_begin (&pipe->commands);

    for (; e != list_end (&pipe->commands); ) {
        struct esh_command *cmd = list_entry(e, struct esh_command, elem);
        e = list_remove(e);
        esh_command_free(cmd);
    }
    free(pipe);
}

void
esh_command_free(struct esh_command * cmd)
{
    char ** p = cmd->argv;
    while (*p) {
        free(*p++);
    }
    if (cmd->iored_input)
        free(cmd->iored_input);
    if (cmd->iored_output)
        free(cmd->iored_output);
    free(cmd->argv);
    free(cmd);
}

#define PSH_MODULE_NAME "esh_module"

/* Load a plugin referred to by modname */
static struct esh_plugin *
load_plugin(char *modname)
{
    printf("Loading %s ...", modname);
    fflush(stdout);

    void *handle = dlopen(modname, RTLD_LAZY);
    if (handle == NULL) {
        fprintf(stderr, "Could not open %s: %s\n", modname, dlerror());
        return NULL;
    }

    struct esh_plugin * p = dlsym(handle, PSH_MODULE_NAME);
    if (p == NULL) {
        fprintf(stderr, "%s does not define %s\n", modname, PSH_MODULE_NAME);
        dlclose(handle);
        return NULL;
    }

    printf("done.\n");
    return p;
}

static bool sort_by_rank (const struct list_elem *a,
                          const struct list_elem *b,
                          void *aux __attribute__((unused)))
{
    struct esh_plugin * pa =  list_entry(a, struct esh_plugin, elem);
    struct esh_plugin * pb =  list_entry(b, struct esh_plugin, elem);
    return pa->rank < pb->rank;
}

/* Load plugins from directory dirname */
void
esh_plugin_load_from_directory(char *dirname)
{
    DIR * dir = opendir(dirname);
    if (dir == NULL) {
        perror("opendir");
        return;
    }

    struct dirent * dentry;
    while ((dentry = readdir(dir)) != NULL) {
        if (!strstr(dentry->d_name, ".so"))
            continue;

        char modname[PATH_MAX + 1];
        snprintf(modname, sizeof modname, "%s/%s", dirname, dentry->d_name);

        struct esh_plugin * plugin = load_plugin(modname);
        if (plugin)
            list_push_back(&esh_plugin_list, &plugin->elem);
    }
    closedir(dir);
}

/* Initialize loaded plugins */
void
esh_plugin_initialize(struct esh_shell *shell)
{
    /* Sort plugins and call init() method. */
    list_sort(&esh_plugin_list, sort_by_rank, NULL);

    struct list_elem * e = list_begin(&esh_plugin_list);
    for (; e != list_end(&esh_plugin_list); e = list_next(e)) {
        struct esh_plugin *plugin = list_entry(e, struct esh_plugin, elem);
        if (plugin->init)
            plugin->init(shell);
    }
}

/* TBD: implement unloading. */

//Wait on the given job
void job_wait(struct esh_pipeline *pipel, struct esh_command_line *cline, struct termios *esh_tty) {
    int state;
    pid_t wait = waitpid(-1, &state, WUNTRACED|WNOHANG);
    if (wait) {
    	int pgrp = getpgrp();
        give_terminal_to(pgrp, esh_tty);
        adjust_state(wait, state);
    }
}

//Adjust the state the given job
void adjust_state(pid_t job, int state) {

//	printf("Adjust State; Job: %d\tState: %d\n", job, state);

	if (job == -1) {
		esh_sys_fatal_error("Unable to wait for job\n");

	} else if (job > 0) {
//		printf("Adjusting job state\n");
        struct list_elem *e = list_begin(&jobList);
        for (; e != list_end(&jobList); e = list_next(e)) {

            struct esh_pipeline *pipel = list_entry(e, struct esh_pipeline, elem);

            if (pipel->pgrp == job) {
            	//we found the current job
//            	printf("jobs state: %d\n", state);

            	if (WIFSTOPPED(state)) {
            		printf("WIFSTOPPED\n");
            		if (WSTOPSIG(state) == 22) {
            			pipel->status = STOPPED;
            	    } else {
            	    	pipel->status = STOPPED;
            	    	printf("\n[%d]+ Stopped      ", pipel->jid);
            	    	jobs_print(jobList);
            	    }
            	}

            	if (WTERMSIG(state) == 9) {
//            		printf("WTERMSIG\n");
            		list_remove(e);
            	} else if (WIFEXITED(state)) {
//            		printf("WIFEXITED\n");
            		list_remove(e);
            	} else if (WIFSIGNALED(state)) {
//            		printf("WIFSIGNALED\n");
            		list_remove(e);
            	} else if (WIFCONTINUED(state)) {
            		list_remove(e);
            	}

            	if (list_empty(&jobList)) {
            		currJobID = 0;
            	}
            }
        }
    }
}

//Parse the command and return a value corresponding to the known built in command
int parse_cmd(char *cmd) {
	int is_builtin = -9;
//	printf("Other is init\n");
	//strcmp returns 0 on match so we need to invert result
	if (!strcmp(cmd, INHOUSE[BG])) {
        is_builtin = BG;
	} else if (!strcmp(cmd, INHOUSE[FG])) {
		is_builtin = FG;
    } else if (!strcmp(cmd, INHOUSE[JOBS])) {
    	is_builtin = JOBS;
    } else if (!strcmp(cmd, INHOUSE[STOP])) {
    	is_builtin = STOP;
    } else if (!strcmp(cmd, INHOUSE[KILL])) {
    	is_builtin = KILL;
    } else {
    	//Identify as plugin?
    }

    return is_builtin;
}

//Print the current list of jobs
void list_jobs() {
	struct list_elem *e = list_begin(&jobList);
	for (; e != list_end(&jobList); e = list_next(e)) {
		struct esh_pipeline *pipeline = list_entry(e, struct esh_pipeline, elem);
		printf("[%d] %s ", pipeline->jid, STATES[pipeline->status]);
		pipeline_print(pipeline);
	}
}

//Handle the SIGCHLD event
void sigchld_event_handler(int sig, siginfo_t *info, void *_ctxt) {
    if (sig == SIGCHLD) {
    	int state;
    	pid_t wait;
//    	printf("SIGCHILD HANDLER\n");
    	while ((wait = waitpid(-1, &state, WUNTRACED|WNOHANG)) > 0) {
    		//do something
//    		printf("Adjusting state inside SIGCHILD");
    		adjust_state(wait, state);
    	}
//    	wait = waitpid(-1, &state, WUNTRACED|WNOHANG);
////    	printf("wait: %d\n", wait);
//    	if (wait > 0) {
//    		//do something
////    		printf("Adjusting state inside SIGCHILD");
//    		adjust_state(wait, state);
//    	}
//    	printf("Sigchild\n");
    }
    //do nothing
}
