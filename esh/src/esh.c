/*
 * esh - the 'pluggable' shell.
 *
 * Developed by Godmar Back for CS 3214 Fall 2009
 * Virginia Tech.
 */
#include <stdio.h>
#include <readline/readline.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "esh-sys-utils.h"
#include "esh.h"

static void
usage(char *progname)
{
    printf("Usage: %s -h\n"
        " -h            print this help\n"
        " -p  plugindir directory from which to load plug-ins\n",
        progname);

    exit(EXIT_SUCCESS);
}

int cmd_cnt = 0;

/* Build a prompt by assembling fragments from loaded plugins that
 * implement 'make_prompt.'
 *
 * This function demonstrates how to iterate over all loaded plugins.
 */
static char *
build_prompt_from_plugins(void)
{
    char *prompt = NULL;
    struct list_elem * e = list_begin(&esh_plugin_list);

    for (; e != list_end(&esh_plugin_list); e = list_next(e)) {
        struct esh_plugin *plugin = list_entry(e, struct esh_plugin, elem);

        if (plugin->make_prompt == NULL)
            continue;

        /* append prompt fragment created by plug-in */
        char * p = plugin->make_prompt();
        if (prompt == NULL) {
            prompt = p;
        } else {
            prompt = realloc(prompt, strlen(prompt) + strlen(p) + 1);
            strcat(prompt, p);
            free(p);
            ///Hello
        }
    }

    /* default prompt */
    if (prompt == NULL)
        prompt = strdup("esh> ");

    return prompt;
}

//Job Control methods
static struct list * get_job_list(void);
static struct esh_pipeline * job_by_pgrp(pid_t);
static struct esh_pipeline * job_by_id(int job_id);

/* The shell object plugins use.
 * Some methods are set to defaults.
 */
struct esh_shell shell = {
    .build_prompt = build_prompt_from_plugins,
    .readline = readline,       /* GNU readline(3) */
    .parse_command_line = esh_parse_command_line, /* Default parser */
    .get_jobs = get_job_list,
    .get_job_from_jid = job_by_id,
    .get_job_from_pgrp = job_by_pgrp
};

int main(int ac, char *av[]) {
    currJobID = 0;
    int opt;
    list_init(&esh_plugin_list);

    setpgid(0, 0);
    struct termios *esh_tty = esh_sys_tty_init();
    give_terminal_to(getpgrp(), esh_tty);

    list_init(&jobList);

    /* Process command-line arguments. See getopt(3) */
    while ((opt = getopt(ac, av, "hp:")) > 0) {
        switch (opt) {
        case 'h':
            usage(av[0]);
            break;

        case 'p':
            esh_plugin_load_from_directory(optarg);
            break;
        }
    }

    esh_plugin_initialize(&shell);

    /* Read/eval loop. */
    //This is a new comment
    //This line is conflicting
    for (;;) {
        /* Do not output a prompt unless shell's stdin is a terminal */
        char * prompt = isatty(0) ? shell.build_prompt() : NULL;
        char * cmdline = shell.readline(prompt);
        free (prompt);
        //Check for "jobs" command
//        if (strcmp(cmdline, "jobs")==0)
//        {
//            esh_job_print();
//        }
        if (cmdline == NULL)  /* User typed EOF */
            break;

        struct esh_command_line * cline = shell.parse_command_line(cmdline);
        free (cmdline);
        if (cline == NULL) {                  /* Error in command line */
            continue;
        }

        if (list_empty(&cline->pipes)) {    /* User hit enter */
            esh_command_line_free(cline);
            continue;
        }

//        simple_print(cline);
//        simple_exec(cline);

        exec_pipeline(cline, esh_tty);
        esh_command_line_free(cline);
    }
    return 0;
}
////////////////////// Exec /////////////////////////////////



//Fancy exec
void exec_pipeline(struct esh_command_line *cline, struct termios *esh_tty) {

    /*Processing pipelines*/
//    struct list_elem * e = list_begin (&cline->pipes);

//    printf("Execing pipeline\n");

//    for (; e != list_end (&cline->pipes); e = list_next (e)) {
//    	printf("pipes\n");

        struct esh_pipeline *pipel = list_entry(list_begin (&cline->pipes), struct esh_pipeline, elem);
//        struct list_elem * c = list_begin(&pipel->commands);
//        for (;c != list_end(&pipel->commands); c = list_next(c)) {
        	cmd_cnt++;
//        	printf("command: %d\n", cmd_cnt);
//        	int cmds = list_size(&pipel->commands);
//        	printf("commands: %d\n", cmds);
        	struct esh_command *cmd = list_entry(list_begin(&pipel->commands), struct esh_command, elem);
//        	printf("Command: %s\n", cmd->argv[0]);
        	int cmd_type = parse_cmd(cmd->argv[0]);

        	switch(cmd_type) {
        		case STOP:
        		case KILL:
        		case BG:
        		case FG:
        			if (!list_empty(&jobList)) {

        				int jid = -1;

        				if (cmd->argv[1] == NULL) {
        					struct list_elem *e = list_back(&jobList);
        					struct esh_pipeline *pipeline = list_entry(e, struct esh_pipeline, elem);
        					jid = pipeline->jid;
        				}

        				else {

        					if (strncmp(cmd->argv[1], "%", 1) == 0) {
        						char *temp = (char*) malloc(5);
        						strcpy(temp, cmd->argv[1]+1);
        						jid = atoi(temp);
        						free(temp);
        					}

        					else {
        						jid = atoi(cmd->argv[1]);
        					}
        				}

        				struct esh_pipeline *pipel = job_by_id(jid);

        				switch(cmd_type) {
        					case STOP:
        						stop_cmd(pipel);
        						break;
        					case KILL:
        						kill_cmd(pipel);
        						break;
        					case BG:
        						bg_cmd(pipel);
        						break;
        					case FG:
        						fg_cmd(pipel, cline, esh_tty);
        						break;
        				}

        			}

        			break;
        		case JOBS:
        			if (!list_empty(&jobList)) {
        				list_jobs();
        			}
        			break;
        		default:
        			//cmd_typ = 0, not a built in command
//        			printf("Default case\n");
        			external_cmd(pipel, cline, esh_tty);
           			break;
        	}
        	//end switch

//        }

//    }
}

/////////////////////// Builtin Functions ////////////////////

//Stop the job
void stop_cmd(struct esh_pipeline *pipel) {
	if (kill(-pipel->pgrp, SIGSTOP) == -1) {
		esh_sys_fatal_error("SIGSTOP Error ");
	}
}

//kill the job
void kill_cmd(struct esh_pipeline *pipel) {
	if (kill(-pipel->pgrp, SIGKILL) < 0) {
		esh_sys_fatal_error("SIGKILL Error ");
	}
}

//bg the job
void bg_cmd(struct esh_pipeline *pipel) {
	pipel->status = BACKGROUND;

	if (kill(-pipel->pgrp, SIGCONT) < 0) {
		esh_sys_fatal_error("SIGCONT Error ");
	}

	jobs_print(jobList);
	printf("\n");
}

//fg the job
void fg_cmd(struct esh_pipeline *pipel, struct esh_command_line *cline, struct termios *esh_tty) {
    esh_signal_block(SIGCHLD);
    pipel->status = FOREGROUND;
    pipeline_print(pipel);
    give_terminal_to(pipel->pgrp, esh_tty);

    if (kill (-pipel->pgrp, SIGCONT) == -1) {
    	esh_sys_fatal_error("fg error: kill SIGCONT ");
    }

    job_wait(pipel, cline, esh_tty);
    esh_signal_unblock(SIGCHLD);
}

//run the external command
void external_cmd(struct esh_pipeline *pipel, struct esh_command_line *cline, struct termios *esh_tty) {
	esh_signal_sethandler(SIGCHLD, sigchld_event_handler);

	if (list_empty(&jobList)) {
		currJobID = 1;
	} else {
		currJobID +=1;
	}
//
	pipel->jid = currJobID;
	pipel->pgrp = -1;
	pid_t child;
	struct list_elem *e = list_begin(&pipel->commands);
	for (; e != list_end(&pipel->commands); e = list_next(e)) {
		struct esh_command *cmd = list_entry(e, struct esh_command, elem);
		esh_signal_block(SIGCHLD);
		child = fork();

		if (child == -1) {
			perror("Unable to fork child process\n");
		}
		else if (child == 0) {
			//Child stuff
			pid_t pid = getpid();
			cmd->pid = pid;
			//We need to set the pgrp to the "parent" process of the pipeline
			if (pipel->pgrp == -1) {
				pipel->pgrp = pid;
			}
			int spgid_status = setpgid(pid, pipel->pgrp);
			if (spgid_status == -1) {
				esh_sys_fatal_error("Unable to set process group of child\n");
			}
			//check for bg state
			if (pipel->bg_job) {
				pipel->status = BACKGROUND;
			}

			//printf("Printing the command\n");
			int exec_status = execvp(cmd->argv[0], cmd->argv);

			if (exec_status == -1) {
                esh_sys_fatal_error("Unable to exec command\n");
			}

		} else {
			//Parent stuff
//			printf("Back in parent\n");
			if (pipel->pgrp == -1) {
				pipel->pgrp = child;
			}

			int spgid_status = setpgid(child, pipel->pgrp);
			if (spgid_status == -1) {
				esh_sys_fatal_error("Unable to set process group of parent\n");
			}

			struct list_elem *j = list_pop_front(&cline->pipes);
			list_push_back(&jobList, j);

			//check bg status
			if (pipel->bg_job) {
				pipel->status = BACKGROUND;
				printf("[%d] %d\n", pipel->jid, pipel->pgrp);
			} else {
				job_wait(pipel, cline, esh_tty);
			}

			esh_signal_unblock(SIGCHLD);
//			printf("Done waiting1\n");
		}
//		esh_signal_unblock(SIGCHLD);
//		printf("Done waiting2\n");

	}


}


//////////////// Helper Funcs ////////////////////


//get the list of jobs
static struct list * get_job_list() {
    return &jobList;
}

//This method will loop over pipe and find the job based on pgrp, if none, return NULL
static struct esh_pipeline * job_by_pgrp(pid_t pgrp) {

	struct list_elem *e= list_begin(&jobList);

    for (; e != list_end(&jobList); e = list_next(e)) {

        struct esh_pipeline *job = list_entry(e, struct esh_pipeline, elem);

        if (job->pgrp == pgrp) {
            //Yay! Found job!
        	return job;
        }
    }

    return NULL;
}

//This method will loop over ths pipeline based on jobid
static struct esh_pipeline * job_by_id(int job_id) {

	//make list
    struct list_elem *e = list_begin(&jobList);
    for (; e != list_end(&jobList); e = list_next(e)) {

        struct esh_pipeline *job = list_entry(e, struct esh_pipeline, elem);

        if (job->jid == currJobID) {
        	//Yay! found job
            return job;
        }
    }

    return NULL;
}


